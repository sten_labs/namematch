# Boilerplate NodeJS app

On the boilerplate branch you should find a very simple skeleton that you can reuse to build a stateless, restful NodeJS backend using MongoDB

There are no views or sessions components implemented. The purpose of this skeleton is to be used as a backend service and you should implement the client side of it in a different mobile or webapp.

## Requirements

* [NodeJS](http://nodejs.org)
* [MongoDB](http://mongodb.org)

## To use

```sh
$ npm install
$ npm start
``

## TODO
[] Move the /login routes to auth/v1
[] Implement the token authentication
[] Implement token expiry
[x] Implement users routes
[x] Implement pagination
[] Implement dummy protected api route
[] Implement dummy public api route 
[x] Implement a test
[] Make a DynamoDB example
