/**
 * Module dependencies
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');
var utils = require ('../../lib/utils');
var PAGE_LIMIT = 25;
var extend = require('util')._extend;

/**
 * Load
 */

exports.load = function (req, res, next, id) {
	var options = {
		criteria: { _id : id },
		select: "name username email"
	};
	User.load(options, function (err, user) {
		if (err) {
			return res.status(500).send({
				error: utils.errors(err.errors),
				user: user
			});
		}
		if (!user) {
			return res.status(500).send({
				error: 'Failed to load User ' + id,
				user: user
			});
		}
		//req.profile = user;
		req.user = user;
		next();
	});
};

/**
 * Create user
 */

exports.create = function (req, res) {
	var user = new User(req.body);
	user.save(function (err){
		if (err) {
			return res.status(400).send({
				error: utils.errors(err.errors),
				user: user
			});
		}
		// removing the hashed password from the result to avoid
		// sending it as JSON
		user.hashed_password = undefined;
		res.status(201).send({
			user: user
		});
	});
};

/**
 * Show a user
 */

exports.show = function (req, res) {
	var user = req.user;
	// Do not send the hashed password
	user.hashed_password = undefined;

	res.status(200).send({
		user: user
	});	
};

/**
 * Update user
 */
exports.update = function (req, res) {
	var user = req.user;

	// Remove the password field. Credentials will have to be updated via a different
	// route

	delete req.body.password;
	user = extend(user, req.body);

	user.validate(function (err){
		if (err) {
			return res.status(500).send({
				error: utils.errors(err.errors),
				user: user
			});
		}
		// Updating the user
		user.save(function (err){
			if (err) {
				return res.status(500).send({
					error: utils.errors(err.errors),
					user: user
				});
			}
			// removing the hashed password from the result to avoid
			// sending it as JSON
			user.hashed_password = undefined;
			res.status(200).send({
				user: user
			});
		});

	});
}

/**
 * Delete user
 */
exports.destroy = function (req, res) {
	var user = req.user;
	user.remove(function (err){
		if (err) {
			return res.status(500).send({
				error: utils.errors(err.errors),
				user: user
			});
		}
		res.status(200).send({
			user: user
		});
	});
}

/**
 * Get the list of users
 */

exports.index = function (req, res) {
	// setting the limit at 25 element per page
	var limit = req.query.limit > 0 ? Math.min(req.query.limit, PAGE_LIMIT) : PAGE_LIMIT;
	// getting the start of the query
	var start = (req.query.start ? mongoose.Types.ObjectId(req.query.start) : mongoose.Types.ObjectId(0));
	var options = {
		perPage: limit,
		criteria: {
			_id: {$gt: start}
		}
	};

	User.list(options, function(err, users) {
		if (err) {
			return res.status(500).send({
				error: utils.errors(err.errors)
			});
		}
		User.count().exec(function (err, count) {
			var nextStart = users.length > 0 ? users[users.length-1]._id : '';
			// TODO: implement last page
			res.status(200).send({
				size: count,
				start: start,
				limit: limit,
				nextStart: nextStart,
				values: users
			});
		});
	});
};
