/**
 * Module dependencies
 */

var mongoose = require('mongoose')
	, should = require('should')
	, request = require('supertest')
	, app = require('../server')
	, context = describe
	, User = mongoose.model('User')

var dummy_user;
var dummy_json = {
	name: "Foo Bar",
	email: "foo@bar.com",
	username: "foobar",
	password: "foobar"
}


describe('Authentication', function(){
	describe ('POST /login', function(){
		describe ('Invalid login', function(){
			before(function (done){
				dummy_user = new User(dummy_json)
				dummy_user.save(function (err){
					if (err){
						throw(err)
					}	
					done()
				})
				
			})
			it('should not login with wrong email', function (done){
				request(app)
				.post('/login')
				.set('Content-type', 'application/json')
				.send({
					email: "bar@foo.com",
					password: "foobar"
				})
				.expect('Content-type', 'application/json; charset=utf-8')
				.expect(401)
				.end(done)
			})
			it('should not login with wrong password', function (done){
				request(app)
				.post('/login')
				.set('Content-type', 'application/json')
				.send({
					email: "foo@bar.com",
					password: "barfoo"
				})
				.expect('Content-type', 'application/json; charset=utf-8')
				.expect(401)
				.end(done)
			})
			it('should login and return a token', function (done){
				request(app)
				.post('/login')
				.set('Content-type', 'application/json')
				.send({
					email: "foo@bar.com",
					password: "foobar"
				})
				.expect('Content-type', 'application/json; charset=utf-8')
				.expect(200)
				.expect(/"token"/)
				.end(done)
			})
		})
	})
	after(function (done) {
    	require('./helper').clearDb(done)
  	})
})