/**
 * Module dependencies
 */

var mongoose = require('mongoose')
	, should = require('should')
	, request = require('supertest')
	, app = require('../server')
	, context = describe
	, User = mongoose.model('User')


var dummy_user;
var dummy_json = {
	name: "Foo Bar",
	email: "foo@bar.com",
	username: "foobar",
	password: "foobar"
}
/**
 * Users test
 */

describe('Users', function(){
	describe('Saving user', function() {
		beforeEach(function (done){
			dummy_user = new User(dummy_json)
			done()
		})
		it('should not save without a password', function (done){
			dummy_user.password = null
			dummy_user.save(function (err, user){
				should.exist(err)
				done()
			})
		})
		it('should not save without an email', function (done){
			dummy_user.email = null
			dummy_user.save(function (err, user){
				should.exist(err)
				done()
			})
		})
		it('should not save without a username', function (done){
			dummy_user.username = null
			dummy_user.save(function (err, user){
				should.exist(err)
				done()
			})
		})
		it('should save User', function (done){
			dummy_user.save(function (err, user){
				should.not.exist(err)
				done()
			})
		})
	})
	after(function (done) {
		require('./helper').clearDb(done)
  	})
})