/**
 * Module dependencies
 */

var mongoose = require('mongoose')
	, should = require('should')
	, request = require('supertest')
	, app = require('../server')
	, context = describe
	, User = mongoose.model('User')

var count

/**
 * Users test
 */

describe('Users', function(){
	describe('POST /users', function() {
		describe('Invalid parameteres', function (){
			before(function (done){
				User.count(function (err, cnt){
					count = cnt
					done()
				})
			})

			it('no email - should repond with errors', function (done) {
				request(app)
				.post('/users')
				.set('Content-type', 'application/json')
				.send({
					name: 'Foo Bar',
					username: 'foo bar',
					email: '',
					password: 'foobar'
				})
				.expect('Content-type', 'application/json; charset=utf-8')
				.expect(400)
				.end(done)
			})

			it('no name - should repond with errors', function (done) {
				request(app)
				.post('/users')
				.set('Content-type', 'application/json')
				.send({
					name: '',
					username: 'foo bar',
					email: 'foo@bar.com',
					password: 'foobar'
				})
				.expect('Content-type', 'application/json; charset=utf-8')
				.expect(400)
				.end(done)
			})

			it('no password - should repond with errors', function (done) {
				request(app)
				.post('/users')
				.set('Content-type', 'application/json')
				.send({
					name: 'Foo Bar',
					username: 'foo bar',
					email: 'foo@bar.com',
					password: ''
				})
				.expect('Content-type', 'application/json; charset=utf-8')
				.expect(400)
				.end(done)
			})

			it('should not have saved any user to the database', function (done){
				User.count(function (err, cnt){
					// comparing count to the new value from the DB
					count.should.equal(cnt)
					done()
				})
			})
		})
		describe('Valid parameters', function(){
			before(function (done){
				User.count(function (err, cnt){
					count = cnt
					done()
				})
			})
			
			it ('should create a new user', function (done){
				request(app)
				.post('/users')
				.set('Content-type', 'application/json')
				.send({
					name: 'Foo Bar',
					username: 'foobar',
					email: 'foo@bar.com',
					password: 'foobar'
				})
				.expect('Content-type', 'application/json; charset=utf-8')
				.expect(201)
				.end(done)
			})

			it('should have inserted only one user in the DB', function (done){
				User.count(function (err, cnt){
					cnt.should.equal(count + 1)
					done()
				})
			})

			it('should have saved the user in the DB', function (done){
				User.findOne({username: 'foobar'}).exec(function (err, user){
					should.not.exist(err)
					user.should.be.an.instanceOf(User)
					user.email.should.equal('foo@bar.com')
					done()
				})
			})
		})
	})
	after(function (done) {
    	require('./helper').clearDb(done)
  	})
})