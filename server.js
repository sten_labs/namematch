/**
 * Main server file
 */

// Module dependencies

var fs = require('fs'); 				//filesystem module
var express = require('express'); 		//Express framework
var mongoose = require('mongoose'); 	//MongoDB connection
var passport = require('passport');
var config = require('config'); 		// Loads https://github.com/lorenwest/node-config

// Creating the app using ExpressJS
var app = express();
var port = process.env.PORT || 3000;
app.set('tokenSecret', 'XqDadq8NGzCUc4');

// Connect to MongoDB
var connect = function (){
	var options = {server: {socketOption: {keepAlive: 1 }}};
	mongoose.connect(config.db, options);
};
connect();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);

// TODO Bootstrap models
fs.readdirSync(__dirname + '/app/models').forEach(function (file) {
	if (~file.indexOf('.js')) require(__dirname + '/app/models/' + file);
});

// Bootstrap passport config
require('./config/passport')(passport, config);

// Bootstrap application settings
require('./config/express')(app, passport);


// Bootstrap routes
require('./config/routes')(app, passport);

app.listen(port)

console.log('Express app started on port ' + port);

/**
 * Expose
 */

module.exports = app;