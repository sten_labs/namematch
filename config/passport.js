/**
 * Module dependencies
 */

var mongoose = require('mongoose');
var User = mongoose.model('User');

var local = require('./passport/local');

/**
 * Expose
 */

module.exports = function(passport, config){
	/* Not sure that I need that
	// Sten.
	//serialize sessions
	passport.serializeUser(function(user, done) {
		done(null, user.id)
	})

	passport.deserializeUser(function(id, done){
		User.findOne({_id: id}, function (err, user){
			done(err, user)
		})
	})
	*/

	passport.use(local);
}