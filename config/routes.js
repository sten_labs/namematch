// Loading controllers

var users = require('users');
var jwt = require('jsonwebtoken');
var utils = require ('../lib/utils');

module.exports = function (app, passport) {
	app.get ('/', function(req, res, next){
		res.json({ message: 'Welcome to the coolest API on earth!' });
	});

	app.post('/login', function(req, res, next) {
		passport.authenticate('local', function(err, user, info) {
			if (err) { return next(err) }
			if (!user) {
				return res.status(401).send({ 
	        		error: 'Couldn\'t find username' 
	        	});
			}
			//user has authenticated correctly thus we create a JWT token 
			var token = jwt.sign({username: user.username, email: user.email}, app.get('tokenSecret'), {
				expiresInMinutes: 1440 
			});

			res.status(200).send({ 
	        	token: token
	        });

		})(req, res, next);
	});

	
	/**
	 * Routes for user creation
	 */
	app.param('userId', users.load);
	app.post('/users', users.create);
	app.get('/users', users.index);
	app.get('/users/:userId', users.show);
	app.put('/users/:userId', users.update);
	app.delete('/users/:userId', users.destroy);



	// require token for all api calls
	app.all('/api/v1/*', function(req, res, next){
		// check header or url parameters or post parameters for token
	    var token = req.body.token || req.query.token || req.headers['x-access-token'];

	    // decode token
	    if (token) {

	    	// verifies secret and checks exp
	    	jwt.verify(token, app.get('tokenSecret'), function(err, decoded) {      
	      		if (err) {
	      			switch (err.name){
	      				case 'TokenExpiredError':
	      					return res.status(401).send({
	       						message: 'Your token has expired' 
	       					});
	      					break;
	      				default:
	      					return res.status(401).send({
	       						message: 'Failed to authenticate token.' 
	       					});
	      			}
	      			    
		    	} else {
		        	// if everything is good, save to request for use in other routes
		        	req.decoded = decoded;
		        	next();
		    	}
		  	});

		} else {

	    	// if there is no token
	    	// return an error
	    	return res.status(400).send({ 
	        	success: false, 
	        	message: 'No token provided.' 
	    	});
	    }
	});

	/**
	* Error handling
	*/

 	app.use(function (err, req, res, next) {
	 	// treat as 404
	    if (err.message
	    	&& (~err.message.indexOf('not found')
	    	|| (~err.message.indexOf('Cast to ObjectId failed')))) {
	    	return next();
	    }
	    console.error(err.stack);
	    // error page
	    res.status(500).send({
	    	status: 500,
	    	error: 'An issue happened while loading the page'
	    });
	});

	// assume 404 since no middleware responded
	app.use(function (req, res, next) {
		res.status(404).send({
			url: req.path,
			error: 'Not found'
	    });
	});
}