// Module dependencies

var express = require('express');
var compression = require('compression');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var winston = require('winston');
var config = require('config');
var pkg = require('../package.json');

var env = process.env.NODE_ENV || 'development';

/**
 * Expose
 */

module.exports = function(app, passport) {

	// Compression middleware, need to be before express.static
	app.use(compression({
		threshold: 512
	}));

	// Static files middleware
	app.use(express.static(config.root + '/public'));

	// Use winston on production
	var log;
	if (env !== 'development'){
		log = {
			stream: {
				write: function (message, encoding) {
					winston.info(message);
				}
			}
		};
	} else {
		log = 'dev';
	}

	// Don't log during tests
	// Logging middleware
	if (env !== 'test') app.use(morgan(log));


	// bodyParser above methodOverride
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(bodyParser.json());
	app.use(methodOverride(function (req, res){
		if (req.body && typeof req.body === 'object' && '_method' in req.body){
			//look in urlencoded POST bodies and delete it
			var method = req.body._method;
			delete req.body._method;
			return method;
		}
	}));

	//using CookieParser
	app.use(cookieParser());

	/*
	// user passport session
	app.use(passport.initialize());
	app.use(passport.session());
	*/
	

};